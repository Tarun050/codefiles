@extends('layouts.app')

@section('content')
<div class="notice">
    <p>Trial period ends in 1 week 6 days from now. <a href=""> Upgrade now </a></p>
</div>
<nav class="navbar navbar-expand-md bg-dark navbar-dark">
    <a class="navbar-brand" href="#">Logo</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                    Content
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">Stories</a>
                    <a class="dropdown-item" href="#">Social posts</a>
                    <a class="dropdown-item" href="#">Media Library</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Calender</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Insights</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                    New
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">New Story</a>
                    <a class="dropdown-item" href="#">New Social post</a>
                    <a class="dropdown-item" href="#">New Compaign</a>
                    <a class="dropdown-item" href="#">Order Content</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Upgrade</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Help</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                    Username
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">Create workspace</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Setting</a>
                    <a class="dropdown-item" href="#">Your profile</a>
                    <a class="dropdown-item" href="#">users</a>
                    <a class="dropdown-item" href="#">billings</a>
                    <a class="dropdown-item" href="#">channel</a>
                    <a class="dropdown-item" href="#">integration</a>
                    <a class="dropdown-item" href="#">view blog</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">refferal program</a>
                    <a class="dropdown-item" href="#">affiliate program</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>

                </div>
            </li>
        </ul>
    </div>
</nav>

<div class="ban">
    <h3>Hi there, Abhishek</h3>
    <div class="dropdown">
        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
            Dropdown button
        </button>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="#">Write a story</a>
            <a class="dropdown-item" href="#">create a social post</a>
            <a class="dropdown-item" href="#">create a compaign</a>
        </div>
    </div>
</div>

<div class="container">
    <div class="whats">
        <h4>What's up now</h4>
    </div>
    <div class="whats2">
        <a href="">
            <p>0</p>
            <span>Stories</span>
        </a>
        <a href="">
            <p>1</p>
            <span>Channels</span>
        </a>
        <a href="">
            <p>0</p>
            <span>Integrations</span>
        </a>
        <div>
            <p>0</p>
            <span>Total Views</span>
        </div>
        <div>
            <p>0</p>
            <span>Total Reads</span>
        </div>
    </div>

    <div class="card basic-card mt-5">
        <div class="card-body">
            <div class="spacing">
                <div class="heading">Recently Edited</div>
                <a href="" class="btn btn-outline-dark">View all</a>
            </div>
            <table class="table mt-4">
                <thead>
                    <tr>
                        <th>Story</th>
                        <th>Status</th>
                        <th>Updated</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Undefined article</td>
                        <td>Draft</td>
                        <td>an hour ago</td>
                        <td>
                            <i class="fa fa-edit"></i>
                            <i class="fa fa-trash-o"></i>
                            <i class="fa fa-ellipsis-h"></i>
                        </td>
                    </tr>
                    <tr>
                        <td>7 tips for [unique topic]</td>
                        <td>Draft</td>
                        <td>an hour ago</td>
                        <td>
                            <i class="fa fa-edit"></i>
                            <i class="fa fa-trash-o"></i>
                            <i class="fa fa-ellipsis-h"></i>
                        </td>
                    </tr>
                    <tr>
                        <td>Press Release: [short description of the big news]</td>
                        <td>Draft</td>
                        <td>an hour ago</td>
                        <td>
                            <i class="fa fa-edit"></i>
                            <i class="fa fa-trash-o"></i>
                            <i class="fa fa-ellipsis-h"></i>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>
@endsection