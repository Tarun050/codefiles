@extends('layouts.app')

@section('content')
<section>
    <div class="login">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="card basic-card card-bg">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <img src="https://wp.localserverweb.com/network/wp-content/uploads/2021/06/logo.png"
                                    alt="logo">
                                <p>Everything is done in one central hub where we work with guest writers. The only
                                    thing I need to do is push that magic green button 'approve' and the campaign is
                                    ready to be launched</p>
                            </div>
                            <div class="col-md-6">
                                <div class="card basic-card">
                                    <div class="card-body">
                                        <h3>Login</h3>
                                        <form method="POST" action="{{ route('login') }}">
                                            @csrf
                                            <div class="form-group">
                                                <label for="email">Email address:</label>
                                                <input type="email"
                                                    class="form-control @error('email') is-invalid @enderror" placeholder="
                                                    Enter email" id="email" name="email" value="{{ old('email') }}"
                                                    required autocomplete="email" autofocus>
                                                @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label for="pwd">Password:</label>
                                                <input type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Enter password"
                                                    id="pwd"  name="password"
                                                    required autocomplete="current-password">
                                                @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="form-group form-check">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" type="checkbox"> Remember me
                                                </label>
                                            </div>
                                            <button type="submit" class="btn btn-primary btn-block">
                                                    {{ __('Login') }}
                                                </button>
                                            <div class="frgt-psw">
                                                @if (Route::has('password.request'))
                                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                                    {{ __('Forgot Your Password?') }}
                                                </a>
                                                @endif
                                            </div>
                                            <div class="login-or">
                                                <span class="or-line"></span>
                                                <span class="span-or">or</span>
                                            </div>
                                            <div class="row">
                                                <div class="col-6">
                                                    <a href="" class="btn btn-block btn-fb">
                                                        <i class="fa fa-facebook"></i> Login
                                                    </a>
                                                </div>
                                                <div class="col-6">
                                                    <a href="" class="btn btn-block btn-google">
                                                        <i class="fa fa-google"></i> Google
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="frgt-psw text-center">
                                                New to Network Story? <a href="{{ route('register') }}">Sign up</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection