@extends('layouts.app')

@section('content')
<section>
    <div class="login">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="card basic-card card-bg">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <img src="https://wp.localserverweb.com/network/wp-content/uploads/2021/06/logo.png"
                                    alt="logo">
                                <p>Everything is done in one central hub where we work with guest writers. The only
                                    thing I need to do is push that magic green button 'approve' and the campaign is
                                    ready to be launched</p>
                            </div>
                            <div class="col-md-6">
                                <div class="card basic-card">
                                    <div class="card-body">
                                        <h3>Get started for free</h3>
                                        <p class="my-font">14 day trial with full access. No credit card required.
                                        </p>
                                        <form method="POST" action="{{ route('register') }}">
                                            @csrf
                                            <div class="form-group">
                                                <input type="text"
                                                    class="form-control @error('name') is-invalid @enderror" name="name"
                                                    value="{{ old('name') }}" required autocomplete="name" autofocus
                                                    placeholder="Enter Name" id="name">

                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <input type="email"
                                                    class="form-control @error('email') is-invalid @enderror"
                                                    name="email" value="{{ old('email') }}" required
                                                    autocomplete="email" placeholder="Enter email" id="email">
                                                @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <input type="password"
                                                    class="form-control @error('password') is-invalid @enderror"
                                                    name="password" required autocomplete="new-password"
                                                    placeholder="Enter password" id="pwd">
                                                @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <input id="password-confirm" type="password" class="form-control"
                                                    name="password_confirmation" required autocomplete="new-password"
                                                    placeholder="Confirm password">

                                            </div>
                                            <div class="form-group form-check">
                                                <label class="form-check-label my-font">
                                                    <input class="form-check-input my-font" type="checkbox"> I agree
                                                    with
                                                    the Privacy Policy, the DPA and the Terms of Service
                                                </label>
                                            </div>
                                            <button type="submit" class="btn btn-primary btn-block">
                                                {{ __('Sign in') }}
                                            </button>
                                            <p class="my-font mt-2">By signing up, I confirm I want to sign up for
                                                the
                                                latest updates from StoryChief.</p>
                                            <div class="login-or">
                                                <span class="or-line"></span>
                                                <span class="span-or">or</span>
                                            </div>
                                            <div class="row">
                                                <div class="col-6">
                                                    <a href="" class="btn btn-block btn-fb">
                                                        <i class="fa fa-facebook"></i> Login
                                                    </a>
                                                </div>
                                                <div class="col-6">
                                                    <a href="" class="btn btn-block btn-google">
                                                        <i class="fa fa-google"></i> Google
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="frgt-psw text-center">
                                                Already have an account? <a href="login.html">Login</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection